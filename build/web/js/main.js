$(document).ready(function() {

  $("#submit").on("click", function(event) {
    $.ajax({
      url: "GetPrimes",
      data: {lowerNumber: 5, upperNumber: 10},
      success: function(result) {
        $("#result").html(result);
      },
      dataType: "html",
      type: "POST"
    });

    event.stopPropagation();
    event.preventDefault();
    return false;
  });

});


