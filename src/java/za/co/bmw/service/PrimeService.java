package za.co.bmw.service;

/**
 * @author theresho
 * @since 04/2017
 */
public abstract class PrimeService {

    /**
     * Returns all prime numbers in a unordered list
     * - using html format
     *
     * @param a - minimum number
     * @param b - maximum number
     * @return results list in html format
     */
    public String calculatePrimes(int a, int b) {
        StringBuilder sb = new StringBuilder("<ul>");

        for (int i = a; i <= b; i++) {
            if (isPrimeNumber(i)) {
                sb.append(String.format("<li>%s</li>", i));
            }
        }
        sb.append("</ul>");

        return sb.toString();
    }

    /**
     * Algorythm to check if number is a prime number or not
     *
     * @param number
     * @return result
     */
    protected abstract boolean isPrimeNumber(int number);
}
