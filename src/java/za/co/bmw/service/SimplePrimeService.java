package za.co.bmw.service;

/**
 * @author theresho
 * @since 04/2017
 */
public class SimplePrimeService extends PrimeService {

    /**
     * Uses a simple algorythm to check if number is a prime
     * - The fundamental theorem of arithmetic establishes the central
     *   role of primes in number theory
     * - any integer greater than 1
     *   can be expressed as a product of primes that is unique up to ordering.
     *   This theorem requires excluding 1 as a prime hence we start from 2
     *
     * @param number
     * @return result
     */
    protected boolean isPrimeNumber(int number) {
        for (int i = 2; i <= number / 2; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * TODO: Use test framework (JUnit) for testing
     *
     * @param args
     */
    public static void main(String args[]) {
        System.out.println(new SimplePrimeService().calculatePrimes(1, 25));
    }
}
