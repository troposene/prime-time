package za.co.bmw.service;

/**
 * @author theresho
 * @since 04/2017
 */
public class SieveORPrimeService extends PrimeService {

    /**
     * @deprecated Sieve of R... does not employ this strategy, use calculatePrimes(*)
     * @param number to check
     * @return result
     */
    protected boolean isPrimeNumber(int number) {
        for (int i = 2; i <= number / 2; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

    /**
     *
     * @param a
     * @param b
     * @return results
     */
    @Override
    public String calculatePrimes(int a, int b) {
        StringBuilder sb = new StringBuilder("<ul>");

        int[] arr = new int[b + 1];
        for (int i = a; i <= Math.sqrt(b); i++) {
            if (arr[i] == 0) {
                for (int j = i * i; j <= b; j += i) {
                    arr[j] = 1;
                    sb.append(String.format("<li>%s</li>", j));
                }
            }
        }
        sb.append("</ul>");
        return sb.toString();
    }

    /**
     * TODO: Use test framework (JUnit) for testing
     *
     * @param args
     */
    public static void main(String args[]) {
        System.out.println(new SimplePrimeService().calculatePrimes(1, 25));
    }
}
