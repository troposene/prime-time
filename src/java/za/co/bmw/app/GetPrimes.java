package za.co.bmw.app;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import za.co.bmw.service.PrimeService;
import za.co.bmw.service.SimplePrimeService;
import za.co.bmw.service.SieveORPrimeService;

@WebServlet(name = "GetPrimes", urlPatterns = {"/GetPrimes"})
public class GetPrimes extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        int lowerNumber = Integer.parseInt(request.getParameter("lowerNumber"));
        int upperNumber = Integer.parseInt(request.getParameter("upperNumber"));

        boolean useSieveOR = "on".equalsIgnoreCase(request.getParameter("useSieveOR")));
        PrimeService servie = useSieveOR ? new SieveORPrimeService() : new SimplePrimeServie();

        long startTime = System.currentTimeMillis();
        String result = servie.calculatePrimes(lowerNumber, upperNumber);
        long endTime = System.currentTimeMillis();
        long timeTaken = endTime - startTime;

        try (PrintWriter out = response.getWriter()) {
            out.println(String.format("Calculated in %sms</b>%s", result));
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
