// Wait for the DOM to be ready
$(document).ready(function() {
  jQuery.validator.addMethod("greaterThanLower", function(value, element) {
      return (parseFloat(value) > parseFloat(document.getElementById('lowerNumber').value));
  }, "Upper number must be greater than lower number");

  $("form[name='prime-time']").validate({
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      lowerNumber: {
        required: true,
        number: true
      },
      upperNumber: {
        required: true,
        number: true,
        greaterThanLower: true
      }
    },
    // Specify validation error messages
    messages: {
      lowerNumber: {
        required: "Please enter a number",
        number: "Please enter a valid number"
      },
      upperNumber: {
        required: "Please enter a number",
        number: "Please enter a valid number"
      }
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      $.ajax({
        url: "GetPrimes",
        data: {lowerNumber:upperNumber, upperNumber:upperNumber, useSieveOR:useSieveOR},
        success: function(result) {
          $("#result").html(result);
        },
        dataType: "html",
        type: "POST"
      });

      event.stopPropagation();
      event.preventDefault();
      return false;
    }
  });
});